using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sheepmovement : MonoBehaviour
{
    // There will be the variables here that represent the sheep's health, speed, breeding threshold and position
    public Transform sheepPoint;
    public float health = 100;
    public float moveSpeed;
    public float breedingthreshold;
    public float turntimer;
    public float goturn;
    public GameObject Sheep;
    public float direction;
    public Transform Spawnpoint;
    public GameObject SheepPrefab;
    public Rigidbody2D rb;
    public Vector2 destination;
    public bool Hungry = false;
    public bool Fear = false;
    public bool Bored = false;
    public bool Replicating = false;
    public float blubber = 0;
    public AudioSource sound;



    Dictionary<int, string> TileValues = new Dictionary<int,string>();
        //Add tile that the sheep is currently on. (Make that value 0.)
        ///While TileValues != <>
            ///Find the tile with the lowest value in the dictionary 
            ///For each of its neighbors, add the value of the tile that the sheep is currently on to the value of its neighbor.
            ///Add that value and the tile idea to the dictionary
            ///Remove the tile that was just considered is on from the dictionary

            ///Get other tile movement values of lowest valued neighbors.
            ///Add tile movement values to the dictionary around the sheep that the sheep has not been to
            ///Tile with the lowest movement value around the sheep = CurrentTarget
            ///Move towards CurrentTarget
            ///Empty dictionary
            ///Rinse and repeat until other.gameObject.CompareTag("Target") is true
            ///Check a heustric like distance between the sheep and the goal.

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, destination, moveSpeed);

        if (health <= 0);
        {
            Destroy(gameObject);
        }
        if (health <= 100)
        {
            Hungry = true;
        }
        if (health <= 100 && blubber < 0);
        {
            blubber = blubber - 1;
            health = health + 1;
        }
        if (blubber >= 15);
        {
            Replicating = true;
        }
        if (health >= 100)
        {
            Hungry = true;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            SceneManager.LoadScene("A Star");
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            SceneManager.LoadScene("Dijkstra's");
        }
        if (Hungry == true);
        {
            ///MoveTowards Grass
        }
        if (Replicating == true);
        {
            GameObject Sheep =Instantiate(SheepPrefab, Spawnpoint.position, Spawnpoint.rotation);
        }
        if (Bored == true);
        {
            sound.Play();
        }
        if (turntimer > 0)
       {
            turntimer -= Time.deltaTime;
       }
        if (turntimer <= 0)
            turntimer = 100000;
            direction = Random.Range (1, 5);
            if (direction == 1)
            {
                rb.AddForce (new Vector2 (0, 100) * Time.deltaTime);
            }
            else if (direction == 2)
            {
                rb.AddForce (new Vector2 (100, 0) * Time.deltaTime);
            }
            else if (direction == 3)
            {
                rb.AddForce (new Vector2 (0, -100) * Time.deltaTime);
            }
            else if (direction == 4)
            {
                rb.AddForce (new Vector2 (-100, 0) * Time.deltaTime);
            }


        //Have a random.range between 1-8 go with each number corresponding to a direction.
        //Have a way to detect sheep that are above the breeding threshold in terms of health.
        //Have a way for the sheep to detect boundaries
        //Have an eating function and being able to tell whether grass exists or not.
        //Find a way to add Dijkstra to this so the sheep avoids obstacles.
    }

    void FixedUpdate()
    {
        if (health <= 0)
            Destroy(gameObject);
    }
     private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Grass") && Hungry == true) 
        {
            health = health + 5;
        }
        if (other.gameObject.CompareTag("Grass") && Hungry != true) 
        {
            blubber = blubber + 5;
        }
        if (other.gameObject.CompareTag("Sheep") && health <= 115);
        {
            Instantiate(SheepPrefab);
            health = health / 2;
        }
        if (other.gameObject.CompareTag("Wolf")) 
        {
            health = health - 10;
        }
        if (other.gameObject.CompareTag("Fence")) 
        {
            moveSpeed = 0f;
        }
        if (other.gameObject.CompareTag("Sheep")) 
        {
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
        ///if (!other.gameObject.CompareTag("Target")) 
        ///{
            ///transform.position = Vector2.MoveTowards(transform.position, destination, moveSpeed);

        ///}
        if (other.gameObject.CompareTag("Target")) 
        {

            ///break;

        }
        
    }
}
