﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wolfmovement : MonoBehaviour
{
    // There will be the variables here that represent the wolf's health, speed, breeding threshold and position
    public Transform Point;
    public float health = 1;
    public float moveSpeed;
    public float breedingthreshold;
    public float turntimer;
    public float goturn;
    public float hunger;
    public GameObject WolfPrefab;
    public float direction;
    public Transform Spawnpoint;
    public Rigidbody2D rb;
    public float boredom;
    public AudioSource sound;
    public float sleepiness = 100;
    // Update is called once per frame
    void Update()
    {
       if (turntimer > 0)
       {
            turntimer -= Time.deltaTime;
       }
        if (turntimer <= 0 && gameObject.tag != "SleepingWolf")
            turntimer = 100000;
            direction = Random.Range (1, 5);
            if (direction == 1)
            {
                rb.AddForce (new Vector2 (0, 100) * Time.deltaTime);
            }
            else if (direction == 2)
            {
                rb.AddForce (new Vector2 (100, 0) * Time.deltaTime);
            }
            else if (direction == 3)
            {
                rb.AddForce (new Vector2 (0, -100) * Time.deltaTime);
            }
            else if (direction == 4)
            {
                rb.AddForce (new Vector2 (-100, 0) * Time.deltaTime);
            }
        if (health <= 0)
        {
            Destroy(gameObject);
        }
        if (boredom <= 0)
        {
            gameObject.tag = "Wolf";
                sound.Play();
        }
        if (sleepiness <= 0)
        {
            gameObject.tag = "SleepingWolf";
            sleepiness -= 1;
        }

        if (hunger >= 50)
        {
            gameObject.tag = "HidingWolf";
        }
        if (hunger <= 50)
        {
            gameObject.tag = "Wolf";
        }
            

        //Have a loop going that does a countdown before doing the wolf's movement.
        //Have a random.range between 1-8 go with each number corresponding to a direction.
        //Have a way to detect wolf that are above the breeding threshold in terms of health.
        //Have a way for the wolf to detect boundaries
        //Have an eating function and being able to tell whether sheep exist or not.
    }
     private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Sheep")) 
        {
            other.gameObject.SetActive(false);
            health = health + 50;
        }
        if (other.gameObject.CompareTag("Wolf") && health >= 115)
        {
            Instantiate(WolfPrefab);
            health = health / 2;
            
        }
        if (other.gameObject.CompareTag("Fence")) 
        {
            moveSpeed = 0f;
        }
        if (other.gameObject.CompareTag("Wolf")) 
        {
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }
}